"use strict";

import * as fs from 'fs'
import { WASI } from 'wasi'

const wasi = new WASI()
const importObject = { wasi_snapshot_preview1: wasi.wasiImport };

(async () => {
  const wasm = await WebAssembly.compile(fs.readFileSync("./function/hello.wasm"));
  const instance = await WebAssembly.instantiate(wasm, importObject);
  wasi.start(instance);

  const sum = instance.exports.add(20, 22);

  console.log("🤖 sum:", sum);

})()
